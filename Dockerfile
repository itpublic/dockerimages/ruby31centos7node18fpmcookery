FROM centos/ruby-27-centos7
USER root

RUN sed -i s/mirror.centos.org/vault.centos.org/g /etc/yum.repos.d/*.repo
RUN sed -i s/^#.*baseurl=http/baseurl=http/g /etc/yum.repos.d/*.repo
RUN sed -i s/^mirrorlist=http/#mirrorlist=http/g /etc/yum.repos.d/*.repo

RUN yum remove -y rh-nodejs\*
RUN yum -y install https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm
# Need to run this to be able to install npm below
RUN curl -sL https://rpm.nodesource.com/setup_16.x | bash -

RUN yum -y install nodejs npm
# Install node 18 since newer versions of Canvas requires this to build
# This overwrites the version of node installed in the previous step
RUN curl -sL 'https://unofficial-builds.nodejs.org/download/release/v18.18.1/node-v18.18.1-linux-x64-glibc-217.tar.xz' | xzcat | tar -vx  --strip-components=1 -C /usr/local/
RUN curl -sL https://dl.yarnpkg.com/rpm/yarn.repo | tee /etc/yum.repos.d/yarn.repo
RUN yum -y install yarn
RUN yum -y install epel-release
RUN yum -y remove \
    postgresql-server \
    postgresql-devel \
    postgresql-libs
RUN yum -y update

RUN yum -y remove rh-ruby27\*

RUN yum -y install wget

RUN wget https://github.com/postmodern/ruby-install/releases/download/v0.9.1/ruby-install-0.9.1.tar.gz
RUN tar -xzvf ruby-install-0.9.1.tar.gz
WORKDIR /opt/app-root/src/ruby-install-0.9.1
RUN make install
RUN ruby-install --system ruby 3.1.1

RUN yum -y install \
    siege \
    ansible \
    apr-devel \
    apr-util-devel \
    autoconf \
    automake \
    bison \
    ca-certificates \
    gcc \
    gcc-c++ \
    gdbm \
    gdbm-devel \
    git \
    httpd \
    httpd-devel \
    jq \
    libc \
    libm \
    libcurl-devel \
    libffi \
    libffi-devel \
    libidn-devel \
    libstdc++ \
    libtool \
    libtool-ltdl \
    libtool-ltdl-devel \
    libxml2-devel \
    libxslt-devel \
    make \
    openssh-clients \
    openssl \
    openssl-devel \
    postgresql12-server \
    postgresql12-devel \
    postgresql12-libs \
    python36 \
    python36-lxml \
    python36-pygments \
    readline-devel \
    readline-devel \
    rpm-build \
    rpmdevtools \
    rsync \
    sqlite-devel \
    vim \
    wget \
    which \
    xmlsec1 \
    xmlsec1-devel \
    xmlsec1-openssl \
    zlib \
    zlib-devel \
 && yum clean all

RUN gem install -N fpm-cookery

ENV PATH "/usr/pgsql-12/bin:$PATH"

SHELL ["/bin/bash", "-lc"]
CMD ["/bin/bash", "-l"]
